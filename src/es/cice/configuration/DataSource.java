package es.cice.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import es.cice.utils.PropertUtil;

@Stateless
public class DataSource {
  private final static Logger LOGGER = Logger.getLogger(DataSource.class.getName());
  private static Connection conn;

  public static Connection getConnection() {

    if (conn != null) {
      return conn;
    }

    PropertUtil pu = new PropertUtil();

    try {
      Class.forName(pu.getDriver());
      conn = DriverManager.getConnection(pu.getUrl(), pu.getUser(), pu.getPassword());
    } catch (ClassNotFoundException | SQLException e) {
      LOGGER.log(Level.SEVERE, "Error conexion BBDD");
      e.printStackTrace();
    }

    return conn;
  }


}
