package es.cice.model;

public class Usuario {

  private Long id;
  private String nombre;
  private String password;

  public Usuario() {
    super();
  }

  public Usuario(Long id, String nombre, String password) {
    super();
    this.id = id;
    this.nombre = nombre;
    this.password = password;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String toString() {
    return "Usuario [id=" + id + ", nombre=" + nombre + ", password=" + password + "]";
  }

  private Usuario(UsuarioBuilder builder) {
    super();
    this.id = builder.id;
    this.nombre = builder.nombre;
    this.password = builder.password;
  }

  public static class UsuarioBuilder {

    private Long id;
    private String nombre;
    private String password;

    public UsuarioBuilder() {
      super();
    }

    public UsuarioBuilder setId(Long id) {
      this.id = id;
      return this;
    }

    public UsuarioBuilder setNombre(String nombre) {
      this.nombre = nombre;
      return this;
    }

    public UsuarioBuilder setPassword(String password) {
      this.password = password;
      return this;
    }

    public Usuario build() {
      return new Usuario(this);
    }

  }
}
