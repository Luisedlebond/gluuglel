package es.cice.model;

public class Coordenadas {

  private Long id;
  private double latitud;
  private double longitud;

  public Coordenadas() {
    super();
  }

  public Coordenadas(Long id, double latitud, double longitud) {
    super();
    this.id = id;
    this.latitud = latitud;
    this.longitud = longitud;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public double getLatitud() {
    return latitud;
  }

  public void setLatitud(double latitud) {
    this.latitud = latitud;
  }

  public double getLongitud() {
    return longitud;
  }

  public void setLongitud(double longitud) {
    this.longitud = longitud;
  }

  @Override
  public String toString() {
    return "Coordenadas [id=" + id + ", latitud=" + latitud + ", longitud=" + longitud + "]";
  }

  private Coordenadas(CoordenadasBuilder builder) {
    super();
    this.id = builder.id;
    this.latitud = builder.latitud;
    this.longitud = builder.longitud;
  }

  public static class CoordenadasBuilder {

    private Long id;
    private double latitud;
    private double longitud;

    public CoordenadasBuilder() {
      super();
    }

    public CoordenadasBuilder setId(Long id) {
      this.id = id;
      return this;
    }

    public CoordenadasBuilder setLatitud(double latitud) {
      this.latitud = latitud;
      return this;
    }

    public CoordenadasBuilder setLongitud(double longitud) {
      this.longitud = longitud;
      return this;
    }

    public Coordenadas build() {
      return new Coordenadas(this);
    }

  }
}
