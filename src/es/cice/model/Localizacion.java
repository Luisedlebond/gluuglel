package es.cice.model;

public class Localizacion {

  private Long id;
  private String nombre;
  private String direccion;
  private Coordenadas coordenadas;

  public Localizacion() {
    super();
  }

  public Localizacion(Long id, String nombre, String direccion, Coordenadas coordenadas) {
    super();
    this.id = id;
    this.nombre = nombre;
    this.direccion = direccion;
    this.coordenadas = coordenadas;
  }

  public String getDireccion() {
    return direccion;
  }

  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Coordenadas getCoordenadas() {
    return coordenadas;
  }

  public void setCoordenadas(Coordenadas coordenadas) {
    this.coordenadas = coordenadas;
  }

  @Override
  public String toString() {
    return "Localizacion [id=" + id + ", nombre=" + nombre + ", direccion=" + direccion
        + ", coordenadas=" + coordenadas + "]";
  }

  private Localizacion(LocalizacionBuilder builder) {
    super();
    this.id = builder.id;
    this.nombre = builder.nombre;
    this.direccion = builder.direccion;
    this.coordenadas = builder.coordenadas;
  }

  public static class LocalizacionBuilder {

    private Long id;
    private String nombre;
    private String direccion;
    private Coordenadas coordenadas;

    public LocalizacionBuilder() {
      super();
    }

    public LocalizacionBuilder setId(Long id) {
      this.id = id;
      return this;
    }

    public LocalizacionBuilder setNombre(String nombre) {
      this.nombre = nombre;
      return this;
    }

    public LocalizacionBuilder setDireccion(String direccion) {
      this.direccion = direccion;
      return this;
    }

    public LocalizacionBuilder setCoordenadas(Coordenadas coordenadas) {
      this.coordenadas = coordenadas;
      return this;
    }

    public Localizacion build() {
      return new Localizacion(this);
    }

  }
}
