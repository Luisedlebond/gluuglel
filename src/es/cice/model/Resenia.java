package es.cice.model;

import java.time.LocalDateTime;

public class Resenia {

  private Long id;
  private LocalDateTime fecha;
  private String opinion;
  private int calificacion;
  private Localizacion localizacion;
  private Long idUsuario;

  public Resenia() {
    super();
  }

  public Resenia(Long id, LocalDateTime fecha, String opinion, int calificacion, Long idUsuario,
      Localizacion localizacion) {
    super();
    this.id = id;
    this.fecha = fecha;
    this.opinion = opinion;
    this.calificacion = calificacion;
    this.localizacion = localizacion;
    this.idUsuario = idUsuario;
  }


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDateTime getFecha() {
    return fecha;
  }

  public void setFecha(LocalDateTime fecha) {
    this.fecha = fecha;
  }

  public String getOpinion() {
    return opinion;
  }

  public void setOpinion(String opinion) {
    this.opinion = opinion;
  }

  public int getCalificacion() {
    return calificacion;
  }

  public void setCalificacion(int calificacion) {
    this.calificacion = calificacion;
  }

  public Localizacion getLocalizacion() {
    return localizacion;
  }

  public void setLocalizacion(Localizacion localizacion) {
    this.localizacion = localizacion;
  }

  public Long getIdUsuario() {
    return idUsuario;
  }

  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }



  @Override
  public String toString() {
    return "Resenia [id=" + id + ", fecha=" + fecha + ", opinion=" + opinion + ", calificacion="
        + calificacion + ", localizacion=" + localizacion + ", usuario=" + idUsuario + "]";
  }

  private Resenia(ReseniaBuilder builder) {
    super();
    this.id = builder.id;
    this.fecha = builder.fecha;
    this.opinion = builder.opinion;
    this.calificacion = builder.calificacion;
    this.localizacion = builder.localizacion;
    this.idUsuario = builder.idUsuario;
  }

  public static class ReseniaBuilder {

    private Long id;
    private LocalDateTime fecha;
    private String opinion;
    private int calificacion;
    private Localizacion localizacion;
    private Long idUsuario;

    public ReseniaBuilder() {
      super();
    }

    public ReseniaBuilder setId(Long id) {
      this.id = id;
      return this;
    }

    public ReseniaBuilder setFecha(LocalDateTime fecha) {
      this.fecha = fecha;
      return this;
    }

    public ReseniaBuilder setOpinion(String opinion) {
      this.opinion = opinion;
      return this;
    }

    public ReseniaBuilder setCalificacion(int calificacion) {
      this.calificacion = calificacion;
      return this;
    }

    public ReseniaBuilder setLocalizacion(Localizacion localizacion) {
      this.localizacion = localizacion;
      return this;
    }

    public ReseniaBuilder setIdUsuario(Long idUsuario) {
      this.idUsuario = idUsuario;
      return this;
    }

    public Resenia build() {
      return new Resenia(this);
    }

  }
}
