package es.cice.utils;

import java.math.BigInteger;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HMac {
  private String key;
  private PropertUtil pu = new PropertUtil();

  public HMac() {
    super();
    this.key = pu.getKey();
  }

  public String calcHmacSha256(String message) {
    byte[] hmacSha256 = null;
    try {
      Mac mac = Mac.getInstance("HmacSHA256");
      SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
      mac.init(secretKeySpec);
      hmacSha256 = mac.doFinal(message.getBytes("UTF-8"));
    } catch (Exception e) {
      throw new RuntimeException("Failed to calculate hmac-sha256", e);
    }
    return String.format("%032x", new BigInteger(1, hmacSha256));
  }


}
