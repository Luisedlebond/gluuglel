package es.cice.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertUtil {

  private String driver;
  private String url;
  private String user;
  private String password;
  private Properties properties;
  private InputStream inputStream;
  private String key;

  public PropertUtil() {

    inputStream = getClass().getClassLoader().getResourceAsStream("db.properties");

    properties = new Properties();

    try {

      properties.load(inputStream);
      driver = properties.getProperty("driver");
      url = properties.getProperty("url");
      user = properties.getProperty("user");
      password = properties.getProperty("password");
      key = properties.getProperty("key");

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String getDriver() {
    return driver;
  }

  public String getUrl() {
    return url;
  }

  public String getUser() {
    return user;
  }

  public String getPassword() {
    return password;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}
