package es.cice.controller;

import java.util.List;
import java.util.Optional;
import es.cice.model.Usuario;

public interface IUsuarioDao {

  public Usuario addUsuario(Usuario usuario);

  public void deleteUsuario(Usuario usuario);

  public void updateUsuario(Usuario usuario, Long id);

  public List<Usuario> getAllUsuario();

  public Usuario getUsuarioById(Long id);

  Optional<Usuario> getUsuarioByNombre(String nombre);
}
