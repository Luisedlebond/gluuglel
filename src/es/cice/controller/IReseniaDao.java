package es.cice.controller;

import java.util.List;
import es.cice.model.Resenia;

public interface IReseniaDao {
  public Resenia addResenia(Resenia resenia);

  public void deleteResenia(Resenia resenia);

  public void updateResenia(Resenia resenia, Long id);

  public List<Resenia> getAllResenia();

  public Resenia getReseniaById(Long id);

  public List<Resenia> getAllReseniaByUsuario(Long idUsuario);

  public List<Resenia> getAllReseniaByNombreLocalizacion(String nombreLocalizacion);

}
