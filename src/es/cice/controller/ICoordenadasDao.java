package es.cice.controller;

import java.util.List;
import es.cice.model.Coordenadas;

public interface ICoordenadasDao {
  public Coordenadas addCoordenada(Coordenadas coordenadas);

  public void deleteCoordenada(Coordenadas coordenadas);

  public void updateCoordenada(Coordenadas coordenadas, Long id);

  public List<Coordenadas> getAllCoordenadas();

  public Coordenadas getCoordenadaById(Long id);
}
