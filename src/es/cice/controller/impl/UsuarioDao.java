package es.cice.controller.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import es.cice.configuration.DataSource;
import es.cice.controller.IUsuarioDao;
import es.cice.model.Usuario;

@Stateless
public class UsuarioDao implements IUsuarioDao {
  private final static Logger LOGGER = Logger.getLogger(UsuarioDao.class.getName());

  private Long addUsuario(String nombre, String password) {
    String query =
        "INSERT INTO GLUUGLEL.USUARIOS (ID ,NOMBRE, PASSWORD) VALUES(USUARIOS_SEQ.NEXTVAL,?,?)";
    Long userCreated = null;
    try (PreparedStatement s =
        DataSource.getConnection().prepareStatement(query, new String[] {"ID"})) {
      s.setString(1, nombre);
      s.setString(2, password);
      if (s.executeUpdate() > 0) {
        ResultSet generatedKeys = s.getGeneratedKeys();
        if (null != generatedKeys && generatedKeys.next()) {
          userCreated = generatedKeys.getLong(1);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return userCreated;
  }

  @Override
  public Usuario addUsuario(Usuario usuario) {
    usuario.setId(addUsuario(usuario.getNombre(), usuario.getPassword()));
    return usuario;
  }

  @Override
  public void deleteUsuario(Usuario usuario) {

    String query = "DELETE FROM GLUUGLEL.USUARIOS WHERE ID=?";
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setLong(1, usuario.getId());
      s.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void updateUsuario(Usuario usuario, Long id) {

    String query = "UPDATE GLUUGLEL.USUARIOS SET NOMBRE=?, PASSWORD=? WHERE ID=?";
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setString(1, usuario.getNombre());
      s.setString(2, usuario.getPassword());
      s.setLong(3, id);
      s.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public List<Usuario> getAllUsuario() {
    List<Usuario> usuarios = new ArrayList<>();
    String query = "SELECT ID, NOMBRE, PASSWORD FROM GLUUGLEL.USUARIOS";
    try (Statement s = DataSource.getConnection().createStatement();
        ResultSet rs = s.executeQuery(query)) {
      while (rs.next()) {
        usuarios.add(new Usuario.UsuarioBuilder().setId(rs.getLong(1)).setNombre(rs.getString(2))
            .setPassword(rs.getString(3)).build());
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return usuarios;
  }

  @Override
  public Usuario getUsuarioById(Long id) {

    String query = "SELECT ID, NOMBRE, PASSWORD FROM GLUUGLEL.USUARIOS WHERE ID=?";
    Usuario usuario = null;
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setLong(1, id);
      ResultSet rs = s.executeQuery();
      rs.next();
      usuario = new Usuario.UsuarioBuilder().setId(rs.getLong(1)).setNombre(rs.getString(2))
          .setPassword(rs.getString(3)).build();

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return usuario;
  }

  @Override
  public Optional<Usuario> getUsuarioByNombre(String nombre) {

    String query = "SELECT ID, NOMBRE, PASSWORD FROM GLUUGLEL.USUARIOS WHERE NOMBRE=?";
    Usuario usuario = null;
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setString(1, nombre);
      ResultSet rs = s.executeQuery();
      rs.next();
      usuario = new Usuario.UsuarioBuilder().setId(rs.getLong(1)).setNombre(rs.getString(2))
          .setPassword(rs.getString(3)).build();

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return Optional.of(usuario);
  }


}
