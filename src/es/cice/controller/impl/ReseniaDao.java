package es.cice.controller.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import es.cice.configuration.DataSource;
import es.cice.controller.ILocalizacionDao;
import es.cice.controller.IReseniaDao;
import es.cice.model.Localizacion;
import es.cice.model.Resenia;

@Stateless
public class ReseniaDao implements IReseniaDao {
  private final static Logger LOGGER = Logger.getLogger(ReseniaDao.class.getName());

  @Inject
  private ILocalizacionDao localizacionDao;

  private Long addResenia(String opinion, LocalDateTime fecha, int calificacion, Long idUsuario,
      Localizacion localizacion) {
    Long resCreated = null;
    String query =
        "INSERT INTO GLUUGLEL.RESENIAS (ID , FECHA, OPINION, CALIFICACION, USUARIO_ID, LOCALIZACION_ID) VALUES (RESENIAS_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";
    Localizacion locCreated = localizacionDao.addLocalizacion(localizacion);

    try (PreparedStatement s =
        DataSource.getConnection().prepareStatement(query, new String[] {"ID"})) {
      s.setObject(1, Timestamp.valueOf(fecha));
      s.setString(2, opinion);
      s.setInt(3, calificacion);
      s.setLong(4, idUsuario);
      s.setLong(5, locCreated.getId());
      if (s.executeUpdate() > 0) {
        ResultSet generatedKeys = s.getGeneratedKeys();
        if (null != generatedKeys && generatedKeys.next()) {
          resCreated = generatedKeys.getLong(1);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return resCreated;
  }

  @Override
  public Resenia addResenia(Resenia resenia) {
    resenia.setId(addResenia(resenia.getOpinion(), resenia.getFecha(), resenia.getCalificacion(),
        resenia.getIdUsuario(), resenia.getLocalizacion()));
    return resenia;
  }

  @Override
  public void deleteResenia(Resenia resenia) {
    localizacionDao.deleteLocalizacion(resenia.getLocalizacion());
    String query = "DELETE FROM GLUUGLEL.RESENIAS WHERE ID=?";
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setLong(1, resenia.getId());
      s.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void updateResenia(Resenia resenia, Long id) {

    String query =
        "UPDATE GLUUGLEL.RESENIAS SET FECHA=?, OPINION=?, CALIFICACION=?, USUARIO_ID=?, LOCALIZACION_ID=? WHERE ID=?";
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setObject(1, Timestamp.valueOf(resenia.getFecha()));
      s.setString(2, resenia.getOpinion());
      s.setInt(3, resenia.getCalificacion());
      s.setLong(4, resenia.getIdUsuario());
      s.setLong(5, resenia.getLocalizacion().getId());
      s.execute();
      localizacionDao.updateLocalizacion(resenia.getLocalizacion(),
          resenia.getLocalizacion().getId());
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public List<Resenia> getAllResenia() {
    List<Resenia> resenias = new ArrayList<>();
    String query =
        "SELECT ID, FECHA, OPINION, CALIFICACION, USUARIO_ID, LOCALIZACION_ID FROM GLUUGLEL.RESENIAS";

    try (Statement s = DataSource.getConnection().createStatement();
        ResultSet rs = s.executeQuery(query)) {

      while (rs.next()) {
        Localizacion localizacio = localizacionDao.getLocalizacionById(rs.getLong(6));

        resenias.add(new Resenia.ReseniaBuilder().setId(rs.getLong(1))
            .setFecha(rs.getTimestamp(2).toLocalDateTime()).setOpinion(rs.getString(3))
            .setCalificacion(rs.getInt(4)).setIdUsuario(rs.getLong(5)).setLocalizacion(localizacio)
            .build());

      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return resenias;
  }

  @Override
  public Resenia getReseniaById(Long id) {
    Resenia resenia = null;
    String query =
        "SELECT ID, FECHA, OPINION, CALIFICACION, USUARIO_ID, LOCALIZACION_ID FROM GLUUGLEL.RESENIAS WHERE ID=?";
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setLong(1, id);
      ResultSet rs = s.executeQuery();
      rs.next();
      Localizacion localizacio = localizacionDao.getLocalizacionById(rs.getLong(6));

      resenia = new Resenia.ReseniaBuilder().setId(rs.getLong(1))
          .setFecha(rs.getTimestamp(2).toLocalDateTime()).setOpinion(rs.getString(3))
          .setCalificacion(rs.getInt(4)).setIdUsuario(rs.getLong(5)).setLocalizacion(localizacio)
          .build();

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return resenia;
  }

  @Override
  public List<Resenia> getAllReseniaByUsuario(Long idUsuario) {
    List<Resenia> resenias = new ArrayList<>();
    String query =
        "SELECT ID, FECHA, OPINION, CALIFICACION, USUARIO_ID, LOCALIZACION_ID FROM GLUUGLEL.RESENIAS RESENIAS WHERE USUARIO_ID=?";

    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setLong(1, idUsuario);
      ResultSet rs = s.executeQuery();
      rs.next();
      while (rs.next()) {
        Localizacion localizacio = localizacionDao.getLocalizacionById(rs.getLong(6));

        resenias.add(new Resenia.ReseniaBuilder().setId(rs.getLong(1))
            .setFecha(rs.getTimestamp(2).toLocalDateTime()).setOpinion(rs.getString(3))
            .setCalificacion(rs.getInt(4)).setIdUsuario(rs.getLong(5)).setLocalizacion(localizacio)
            .build());

      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return resenias;
  }

  @Override
  public List<Resenia> getAllReseniaByNombreLocalizacion(String nombreLocalizacion) {
    List<Resenia> resenias = new ArrayList<>();
    String query =
        "SELECT r.* FROM LOCALIZACIONES l JOIN RESENIAS r ON r.LOCALIZACION_ID = l.ID WHERE upper(l.NOMBRE) like upper(?)";

    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setString(1, nombreLocalizacion);
      ResultSet rs = s.executeQuery();
      rs.next();
      while (rs.next()) {
        Localizacion localizacio = localizacionDao.getLocalizacionById(rs.getLong(6));

        resenias.add(new Resenia.ReseniaBuilder().setId(rs.getLong(1))
            .setFecha(rs.getTimestamp(2).toLocalDateTime()).setOpinion(rs.getString(3))
            .setCalificacion(rs.getInt(4)).setIdUsuario(rs.getLong(5)).setLocalizacion(localizacio)
            .build());

      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return resenias;
  }

}
