package es.cice.controller.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import es.cice.configuration.DataSource;
import es.cice.controller.ICoordenadasDao;
import es.cice.controller.ILocalizacionDao;
import es.cice.model.Coordenadas;
import es.cice.model.Localizacion;

@Stateless
public class LocalizacionDao implements ILocalizacionDao {
  private final static Logger LOGGER = Logger.getLogger(LocalizacionDao.class.getName());

  @Inject
  ICoordenadasDao coordenadasDao;

  private Long addLocalizacion(String nombre, String direccion, Coordenadas coordenadas) {
    String query =
        "INSERT INTO GLUUGLEL.LOCALIZACIONES (ID , NOMBRE, DIRECCION, COORDENADA_ID) VALUES(LOCALIZACIONES_SEQ.NEXTVAL, ?, ?, ?)";
    Long locCreated = null;
    Coordenadas coorCreated = coordenadasDao.addCoordenada(coordenadas);
    try (PreparedStatement s =
        DataSource.getConnection().prepareStatement(query, new String[] {"ID"})) {
      s.setString(1, nombre);
      s.setString(2, direccion);
      s.setLong(3, coorCreated.getId());
      if (s.executeUpdate() > 0) {
        ResultSet generatedKeys = s.getGeneratedKeys();
        if (null != generatedKeys && generatedKeys.next()) {
          locCreated = generatedKeys.getLong(1);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return locCreated;

  }



  @Override
  public Localizacion addLocalizacion(Localizacion localizacion) {
    localizacion.setId(addLocalizacion(localizacion.getNombre(), localizacion.getDireccion(),
        localizacion.getCoordenadas()));
    return localizacion;
  }

  @Override
  public void deleteLocalizacion(Localizacion localizacion) {
    coordenadasDao.deleteCoordenada(localizacion.getCoordenadas());
    String query = "DELETE FROM GLUUGLEL.LOCALIZACIONES WHERE ID=?";
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setLong(1, localizacion.getId());
      s.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void updateLocalizacion(Localizacion localizacion, Long id) {
    coordenadasDao.updateCoordenada(localizacion.getCoordenadas(),
        localizacion.getCoordenadas().getId());
    String query = "UPDATE GLUUGLEL.LOCALIZACIONES SET NOMBRE=?, DIRECCION=? WHERE ID=?";
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setString(1, localizacion.getNombre());
      s.setString(2, localizacion.getDireccion());
      s.setLong(3, id);
      s.execute();
      coordenadasDao.updateCoordenada(localizacion.getCoordenadas(),
          localizacion.getCoordenadas().getId());
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public List<Localizacion> getAllLocalizacion() {
    List<Localizacion> localizaciones = new ArrayList<>();
    String query = "SELECT ID, NOMBRE, DIRECCION, COORDENADA_ID FROM GLUUGLEL.LOCALIZACIONES";

    try (Statement s = DataSource.getConnection().createStatement();
        ResultSet rs = s.executeQuery(query)) {

      while (rs.next()) {

        Coordenadas coordenada = coordenadasDao.getCoordenadaById(rs.getLong(4));
        localizaciones.add(
            new Localizacion.LocalizacionBuilder().setId(rs.getLong(1)).setNombre(rs.getString(2))
                .setDireccion(rs.getString(3)).setCoordenadas(coordenada).build());

      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return localizaciones;
  }

  @Override
  public Localizacion getLocalizacionById(Long id) {
    String query =
        "SELECT ID, NOMBRE, DIRECCION, COORDENADA_ID FROM GLUUGLEL.LOCALIZACIONES WHERE ID=?";
    Localizacion localizacion = null;

    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setLong(1, id);
      ResultSet rs = s.executeQuery();
      rs.next();
      Coordenadas coordenada = coordenadasDao.getCoordenadaById(rs.getLong(4));
      localizacion =
          new Localizacion.LocalizacionBuilder().setId(rs.getLong(1)).setNombre(rs.getString(2))
              .setDireccion(rs.getString(3)).setCoordenadas(coordenada).build();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return localizacion;
  }

}
