package es.cice.controller.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import es.cice.configuration.DataSource;
import es.cice.controller.ICoordenadasDao;
import es.cice.model.Coordenadas;

@Stateless
public class CoordenadasDao implements ICoordenadasDao {

  private final static Logger LOGGER = Logger.getLogger(CoordenadasDao.class.getName());

  private Long addCoordenada(double latitud, double longitud) {
    String query =
        "INSERT INTO GLUUGLEL.COORDENADAS (ID ,LATITUD, LONGITUD) VALUES(COORDENADAS_SEQ.NEXTVAL, ?, ?)";
    Long coorCreated = null;
    try (PreparedStatement s =
        DataSource.getConnection().prepareStatement(query, new String[] {"ID"})) {
      s.setDouble(1, latitud);
      s.setDouble(2, longitud);
      if (s.executeUpdate() > 0) {
        ResultSet generatedKeys = s.getGeneratedKeys();
        if (null != generatedKeys && generatedKeys.next()) {
          coorCreated = generatedKeys.getLong(1);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return coorCreated;
  }

  @Override
  public Coordenadas addCoordenada(Coordenadas coordenadas) {
    coordenadas.setId(addCoordenada(coordenadas.getLatitud(), coordenadas.getLongitud()));
    return coordenadas;
  }

  @Override
  public void deleteCoordenada(Coordenadas coordenadas) {
    String query = "DELETE FROM GLUUGLEL.COORDENADAS WHERE ID=?";
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setLong(1, coordenadas.getId());
      s.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void updateCoordenada(Coordenadas coordenadas, Long id) {
    String query = "UPDATE GLUUGLEL.COORDENADAS SET LATITUD=?, LONGITUD=? WHERE ID=?";
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setDouble(1, coordenadas.getLatitud());
      s.setDouble(2, coordenadas.getLongitud());
      s.setLong(3, id);
      s.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public List<Coordenadas> getAllCoordenadas() {
    List<Coordenadas> coordenadas = new ArrayList<>();
    String query = "SELECT ID, LATITUD, LONGITUD FROM GLUUGLEL.COORDENADAS";
    try (Statement s = DataSource.getConnection().createStatement();
        ResultSet rs = s.executeQuery(query)) {
      while (rs.next()) {
        coordenadas.add(new Coordenadas.CoordenadasBuilder().setId(rs.getLong(1))
            .setLatitud(rs.getDouble(2)).setLongitud(rs.getDouble(3)).build());
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return coordenadas;
  }

  @Override
  public Coordenadas getCoordenadaById(Long id) {
    String query = "SELECT ID, LATITUD, LONGITUD FROM GLUUGLEL.COORDENADAS WHERE ID=?";
    Coordenadas coordenada = null;
    try (PreparedStatement s = DataSource.getConnection().prepareStatement(query)) {
      s.setLong(1, id);
      ResultSet rs = s.executeQuery();
      rs.next();
      coordenada = new Coordenadas.CoordenadasBuilder().setId(rs.getLong(1))
          .setLatitud(rs.getDouble(2)).setLongitud(rs.getDouble(3)).build();

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return coordenada;
  }


}
