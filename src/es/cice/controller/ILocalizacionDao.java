package es.cice.controller;

import java.util.List;
import es.cice.model.Localizacion;

public interface ILocalizacionDao {
  public Localizacion addLocalizacion(Localizacion localizacion);

  public void deleteLocalizacion(Localizacion localizacion);

  public void updateLocalizacion(Localizacion localizacion, Long id);

  public List<Localizacion> getAllLocalizacion();

  public Localizacion getLocalizacionById(Long id);
}
