package es.cice;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api/v0")
public class GluuglelApplication extends Application {

}
