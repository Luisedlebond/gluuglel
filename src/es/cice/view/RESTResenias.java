package es.cice.view;

import java.util.List;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import es.cice.configuration.GluuglelConfiguration;
import es.cice.controller.IReseniaDao;
import es.cice.model.Resenia;


@Path("/resenia")
public class RESTResenias {
  private final static Logger LOGGER = Logger.getLogger(GluuglelConfiguration.class.getName());

  @Inject
  private IReseniaDao reseniaDao;


  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Resenia getResenia(@PathParam("id") Long id) {
    return reseniaDao.getReseniaById(id);
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Resenia> getResenias() {
    return reseniaDao.getAllResenia();
  }

  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Resenia createResenia(Resenia resenia) {
    reseniaDao.addResenia(resenia);
    return resenia;
  }

  @PUT
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Resenia updateResenia(@PathParam("id") Long id, Resenia resenia) {
    reseniaDao.updateResenia(resenia, id);
    return resenia;
  }

  @DELETE
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response deleteResenia(@PathParam("id") Long id) {
    reseniaDao.deleteResenia(reseniaDao.getReseniaById(id));
    return Response
        .created(UriBuilder.fromResource(RESTResenias.class).path(String.valueOf(id)).build())
        .build();
  }



}
