package es.cice.view.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException> {

  @Override
  public Response toResponse(DataNotFoundException exception) {
    ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), exception.getErrorCode(),
        exception.getDocumentation());
    return Response.status(exception.getErrorCode()).entity(errorMessage).build();
  }

}
