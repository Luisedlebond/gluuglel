package es.cice.view.exceptions;

import javax.ws.rs.core.Response.Status;

public class DataNotFoundException extends RuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private Status errorCode;
  private String documentation;

  public DataNotFoundException(String data, Status errorCode, String documentation) {
    super(data);
    this.errorCode = errorCode;
    this.documentation = documentation;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public Status getErrorCode() {
    return errorCode;
  }

  public String getDocumentation() {
    return documentation;
  }

  public void setErrorCode(Status errorCode) {
    this.errorCode = errorCode;
  }

  public void setDocumentation(String documentation) {
    this.documentation = documentation;
  }



}
