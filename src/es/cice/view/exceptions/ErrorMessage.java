package es.cice.view.exceptions;

import javax.ws.rs.core.Response.Status;

public class ErrorMessage {

  private String errorMessage;
  private Status errorCode;
  private String documentation;

  public ErrorMessage() {}

  public ErrorMessage(String errorMessage, Status errorCode, String documentation) {
    super();
    this.errorMessage = errorMessage;
    this.errorCode = errorCode;
    this.documentation = documentation;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public Status getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(Status errorCode) {
    this.errorCode = errorCode;
  }

  public String getDocumentation() {
    return documentation;
  }

  public void setDocumentation(String documentation) {
    this.documentation = documentation;
  }

}
