package es.cice.view;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import es.cice.configuration.GluuglelConfiguration;
import es.cice.controller.IReseniaDao;
import es.cice.controller.IUsuarioDao;
import es.cice.model.Resenia;
import es.cice.model.Usuario;
import es.cice.view.exceptions.DataNotFoundException;


@Path("/usuario")
public class RESTUsuario {
  private final static Logger LOGGER = Logger.getLogger(GluuglelConfiguration.class.getName());

  @Inject
  private IUsuarioDao usuarioDao;
  @Inject
  private IReseniaDao reseniaDao;

  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Usuario getUsuario(@PathParam("id") Long id) {

    return Optional.ofNullable(usuarioDao.getUsuarioById(id))
        .orElseThrow(() -> new DataNotFoundException("No se encontro el usuario " + id,
            Response.Status.NOT_FOUND, "Data not Found"));
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Usuario> getUsuarios() {
    return usuarioDao.getAllUsuario();
  }

  @GET
  @Path("/{id}/resenias")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Resenia> getReseniaByUsuario(@PathParam("id") Long id) {
    return reseniaDao.getAllReseniaByUsuario(id);
  }


  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Usuario createUsuario(Usuario usuario) {
    usuario = usuarioDao.addUsuario(usuario);
    return Optional.ofNullable(usuario)
        .orElseThrow(() -> new DataNotFoundException("No se ha creado el usuario",
            Response.Status.CONFLICT, "Resource not created"));
  }

  @PUT
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Usuario updateUsuario(@PathParam("id") Long id, Usuario usuario) {
    usuarioDao.updateUsuario(usuario, id);
    return usuario;
  }

  @DELETE
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response deleteUsuario(@PathParam("id") Long id) {
    usuarioDao.deleteUsuario(usuarioDao.getUsuarioById(id));
    return Response
        .created(UriBuilder.fromResource(RESTUsuario.class).path(String.valueOf(id)).build())
        .build();

  }


}
