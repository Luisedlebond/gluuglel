package es.cice.view;

import java.util.List;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import es.cice.controller.ICoordenadasDao;
import es.cice.controller.ILocalizacionDao;
import es.cice.controller.IReseniaDao;
import es.cice.model.Coordenadas;
import es.cice.model.Localizacion;
import es.cice.model.Resenia;


@Path("/localizacion")
public class RESTLocalizaciones {
  private final static Logger LOGGER = Logger.getLogger(RESTLocalizaciones.class.getName());

  @Inject
  private ILocalizacionDao localizacionDao;
  @Inject
  private ICoordenadasDao coordenadasDao;
  @Inject
  private IReseniaDao reseniaDao;

  @POST
  @Path("/coordenada")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Coordenadas createCoordenada(Coordenadas coordenadas) {
    coordenadasDao.addCoordenada(coordenadas);
    return coordenadas;
  }

  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Localizacion createLocalizacion(Localizacion localizacion) {
    localizacionDao.addLocalizacion(localizacion);
    return localizacion;
  }

  @GET
  @Path("/{nombreLocalizacion}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Resenia> getResenia(@PathParam("nombreLocalizacion") String nombreLocalizacion) {
    return reseniaDao.getAllReseniaByNombreLocalizacion(nombreLocalizacion);
  }
}
